# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

# set you own data here

pieces = {
    ### 000-199 : notes (mêmes numéro que pitch MIDI, do médium = 60)
    '060' : [  #ok
              ['D36C1899'],
              ['672e2621'],
              ['33961f99'],
              ['83a825ab'],
              ['f347ceab'], # Gouttes fines jaunes

              ['93c51899'], # Goutte bleue (épaisse)
          ],
    '062' : [ # => ré
              ['13F6C3A3'],
              ['b7ff2a21'],
              ['6300ceab'], # Gouttes fines jaunes
          ],
    '064' : [ # => mi
              ['87BB1C21'],
              ['7338C4A3'],
              ['13aec3a3'],
              ['27d92d21'],
              ['134166ab'], # Gouttes fines jaunes
              ['03e71e99'], # Goutte bleue
          ],
    '067' : [ # => sol
              ['93AA0DAB'],
              ['67902921'],
              ['531DC4A3'],
              ['634ec3a3'],
              ['b7b22821'],
              ['03dcceab'],
              ['3361ceab'], # Gouttes fines jaunes
              ['83981e99'], # Goutte orange
              ['f35b2199'], # Goutte bleue

         ],
    '071' : [ # => si
              ['57372321'],
              ['5323c2a3'],
              ['23cdceab'], # Gouttes fines jaunes
              ['03b01e99'], # Goutte orange
          ],
    '072' : [ # => do
              ['47D52821'],
              ['23d81999'],
              ['939dcfab'],
              ['27792821'],
              ['a7b82c21'], # Goutte orange
              ['23262099'], # Goutte bleue
         ],
    '074' : [ # => ré
              ['D7C33321'],
              ['13831f99'],
              ['c3ed1799'], # Goutte orange
          ],

    #~ ### énoncés de la dictée de notes
    'd-1' : [
        ['132e66ab'],
        ['9339dbab'],
        ['a235c656'],
        ['234abaab'], # Enoncé jaune
    ],
    'd-2' : [
        ['93a1cfab'],
        ['23fd1e99'],
        ['57c43321'], # ennonce orange
    ],
    'd-3' : [
        ['63ab74ab'],
        ['57191e21'],
        ['d7a51e21'], #ennonce bleu
    ],


    'defi-1': [
        ['f3581f99'], # Enoncé jaune
    ],

    'defi-2': [
        ['f332c3a3'], #67
        ['c7751b21'], # ennonce orange
    ],

    'defi-3': [
        ['337b1899'], # ennonce bleu
    ],

    # Frère Jacques # ok
    'fj' : [ ['57242621'],
             ['e7643421'] #Enoncé jaune : pas toujours reconnu
         ],
    'fj-1': [ ['23cbc2a3'], ['83321b99'],
              ['1346fcaa'],# Mélodie longue jaune : pas toujours reconnu
         ],
    'fj-2': [ ['c4d15632'],
              ['23B3CEAB'], # Mélodie longue jaune  : pas toujours reconnu
         ],
    'fj-3': [ ['d3ae2299'],
              ['23371f99'],# Mélodie longue jaune
         ],
    'fj-4': [ ['97d02521'],
              ['33721c99'],# Mélodie longue jaune
         ],

    # Pont d'Avignon
    'pa' : [ ['879E2521'],
             ['4331cfab'] # Enoncé jaune
         ],
    'pa-1': [ ['1309E2AB'],
              ['c333cdab'], # Mélodie courte jaune
         ],
    'pa-2': [ ['4316C2A3'],
              ['13e71f99'], # Mélodie courte jaune
         ],
    'pa-3': [ ['D7343021'],
              ['57e32c21'], # Mélodie courte jaune
         ],
    'pa-4': [ ['D324DBAB'],
              ['836d1899'],# Mélodie courte jaune
         ],

    # À la claire fontaine
    'cf' : [ ['739B73AB'],
         ],
    'cf-1': [ ['F7472621'],
         ],
    'cf-2': [ ['133B71AB'],
         ],
    'cf-3': [ ['57F92D21'],
         ],
    'cf-4': [ ['87DA2A21'],
         ],

    # Boléro
    'bo' : [ ['F7F12F21'],
             ['23401f99'], # Enoncé orange
             ['87702621'],
         ],
    'bo-1': [ ['13071F99'],
              ['43a92299'], # Mélodie orange (2 courtes + 2 longues)
         ],
    'bo-2': [ ['73F2C3A3'],
              ['f30c2199'], # Mélodie orange (2 courtes + 2 longues)
         ],
    'bo-3': [ ['637175AB'],
              ['53211999'], # Mélodie orange (2 courtes + 2 longues)
         ],
    'bo-4': [ ['E3AFC2A3'],
              ['a3151d99'], # Mélodie orange (2 courtes + 2 longues)
         ],

    ### similarite
    's-00': [ ['d3d1c2a3'],
              ['97912d21'],
              ['7203c056'],
              ['03971799'], # modele A
          ],
    's-01': [ ['871b2a21'],
              ['e345cdab'],
              ['f7c42521'], # jaune
              ['37fc1f21'],
          ],
    's-02': [ ['s-02'],
              ['83001799'], #jaune
          ],
    's-03': [ ['23cd1a99'], # pupitre jaune
              ['a3f4b9ab'],
          ],
    's-04': [ ['476e2621'], # pupitre orange
          ],
    's-05': [ ['73731999'], # pupitre orange
          ],
    's-06': [ ['23ff1d99'], # pupitre bleu
          ],



    's-10': [ ['172c1721'],
              ['430c0fab'],
              ['925bc556'],
              ['67342721'], # goutte jaune TEST
              ['73edfeaa'], # modele B
          ],
    's-11': [ ['23921899'],
              ['73751d99'],
              ['83e7cdab'], # jaune
              ['6221c856'],
          ],
    's-12': [ ['s-12'],
              ['a39e2199'], # jaune
              ['77a51721'],
          ],
    's-13': [
              ['f30473ab'], # pupitre jaune
          ],
    's-14': [ ['f7f51a21'], # pupitre orange
          ],
    's-15': [ ['b31a1899'], # pupitre bleu
          ],
    's-16': [ ['230b2099'],
          ],
}

notes = ['060',
         '062',
         '064',
         '067',
         '071',
         '072',
         '074']
