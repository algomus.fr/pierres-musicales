# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import logging
import logging.config

import defs
import jeu

ZONE_ECOUTE = 1
ZONE_UN = 2
ZONE_DEUX = 3
ZONE_DESACTIVEE = 0

zones  = [[ZONE_ECOUTE, ZONE_ECOUTE, ZONE_ECOUTE, ZONE_ECOUTE],
          [ZONE_UN, ZONE_UN, ZONE_DEUX, ZONE_DEUX],
          [ZONE_UN, ZONE_UN, ZONE_DEUX, ZONE_DEUX],
          [ZONE_UN, ZONE_UN, ZONE_DEUX, ZONE_DEUX],
         ]

def get_pieces_a_ecouter(piece_id_by_zone):
    pl = []
    for piece_id in piece_id_by_zone[ZONE_ECOUTE]:
        if piece_id is not None:
            pl.append(piece_id)
    return [pl]

def get_pieces_proposition(piece_id_by_zone):
    proposition = []
    piece_id_by_zone_keys = piece_id_by_zone.keys()
    piece_id_by_zone_keys.sort()
    for num_zone in piece_id_by_zone_keys:
        if num_zone != ZONE_ECOUTE:
            piece_id_list = piece_id_by_zone[num_zone]
            pl = []
            for piece_id in piece_id_list:
                if piece_id is not None:
                    pl.append(piece_id)
            proposition.append(pl)
    return proposition

def jouer(sim_id):
    logging.getLogger("user").info("*Nouvelle similarite...")
    partie = jeu.JeuDeTri(zones=zones,
                          simi_id=sim_id)
    partie.jouer_debutPartie()

    commande = None
    partieTerminee = False
    partieGagnee = False
    while not partieTerminee:
        logging.getLogger().debug("Attente de commande....")
        logging.getLogger("user").info("Depose tes pieces et appuie sur le bouton")
        commande = partie.get_commande()

        logging.getLogger().debug("Commande = %s" % commande)
        piece_id_by_zone = partie.get_piece_id_by_zone()
        pieces_a_ecouter = get_pieces_a_ecouter(piece_id_by_zone)
        proposition = get_pieces_proposition(piece_id_by_zone)

        # listes des pieces deposees
        all_pieces_id = []
        for l in pieces_a_ecouter+proposition:
            for p_id in l:
                all_pieces_id.append(p_id)
        logging.getLogger().debug("(pieces_a_ecouter, proposition, all_pieces_id) = (%s, %s, %s)" % (pieces_a_ecouter, proposition, all_pieces_id))
        logging.getLogger("user").info("   (pieces_a_ecouter, proposition, all_pieces_id) = (%s, %s, %s)" % (pieces_a_ecouter, proposition, all_pieces_id))
        if commande == jeu.CMD_ECOUTER:
            if len(all_pieces_id) == 0:
                logging.getLogger("user").info("Pas de piece posee ou detectees")
            else:
                # nouveau jeu a commencer ?
                sim_id_deposees = jeu.piece_ids_2_jeu_id(all_pieces_id)
                logging.getLogger().debug("sim_id_deposees=%s" % sim_id_deposees)
                if sim_id_deposees is None:
                    logging.getLogger("user").info("********************** Ennonce non reconnu (incomplet ?) ***********")
                elif sim_id_deposees != sim_id:
                    logging.getLogger("user").info("Nouveau jeu detecte...")
                    sim_id = sim_id_deposees
                    partieTerminee = True
                # des pieces a ecouter ?
                elif len(pieces_a_ecouter[0]) > 0:
                    logging.getLogger("user").info("Ecoute les pieces deposees sur la zone d'ecoute...")
                    partie.jouer_proposition(pieces_a_ecouter)
                # verifions...
                else:
                    logging.getLogger("user").info("Verifions tes %s piece(s)..." % len(all_pieces_id))
                    gagne = partie.a_gagne(proposition)
                    if gagne > 0:
                        logging.getLogger("user").info("********************** BRAVO! ***********************")
                        partie.jouer_gagner()
                        partieTerminee = True
                        partieGagnee = False
                    elif gagne == 0:
                        logging.getLogger("user").info("********************** CORRECT! *********************")
                        partie.jouer_correct()
                    else:
                        logging.getLogger("user").info("********************** ESSAIE ENCORE... *************")
                        partie.jouer_perdu()
            #end if len(all_pieces_id) == 0:
        # end if commande == jeu.CMD_ECOUTER:
    #end while
    del partie
    return (sim_id, partieGagnee)
