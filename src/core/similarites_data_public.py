# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

# Pour chaque niveau, il y a deux groupes
#similarites = {
#    X : [['xm0', 'xm1'],  # pieces modèles 0 et modèle 1 respectivement pour le tas 0 et le tas 1
#         {
#            X1: [['xa'],       # piece(s) similaire(s) au modèle xm0
#                 ['xb'],       # piece(s) similaire(s) au modèle xm1
#                 ],
#            X2: (['xc'],       # pieces similaire(s) au modèle xm0
#                 ['xd', 'xe'], # pieces similaire(s) au modèle xm1
#                 ],
#         }
#    Y : [['ym0', 'ym1'],       # pieces modèles 0 et modèle 1) respectivement pour le tas 0 et le tas 1
#         {
#            Y1: [['ya'],       # pieces similaire(s) au modèle ym0
#                 ['yb'],       # pieces similaire(s) au modèle ym1
#                 ],
#            Y2: [['yc', 'yd'], # pieces similaire(s) au modèle ym0
#                 ['ye', 'yf'], # pieces similaire(s) au modèle ym1
#                 ],
#         }
#}

similarites = {
    # s-00 et s-10: modèles (couleur particulière)
    # s-01 et s-11: copie exacte, mais couleur standard
    30: [['s-00', 's-10'],  # pieces modèles 0 et modèle 1 respectivement pour le tas 0 et le tas 1
         {
            31: [['s-01'], # pieces similaires au modèle 0
                 ['s-11'], # pieces similaires au modèle 1
                 ],

            # Transpositions
            32: [['s-02', 's-03'],
                 ['s-12', 's-13'],
                 ],

            # Fragmentation simple
            33: [['s-04', 's-05'],
                 ['s-14'],
                 ],

            # Fragmentation plus élaborée
            34: [['s-06'],
                 ['s-15', 's-16'],
                 ],

            # Ci-dessous, une version 'avec accumulation', ou on laisse
            # les pièces des niveaux précédents
            # (car tout est similaire soit à s-00, soit à s-10)

            # Transpositions (31 U 32)
            35: [['s-01', 's-02', 's-03'],
                 ['s-11', 's-12', 's-13'],
                ],

            # Fragmentation simple
            36: [['s-01', 's-02', 's-03', 's-04', 's-05'],
                 ['s-11', 's-12', 's-13', 's-14'],
                 ],

            # Fragmentation plus élaborée
            37: [['s-01', 's-02', 's-03', 's-04', 's-05', 's-06'],
                 ['s-11', 's-12', 's-13', 's-14', 's-15', 's-16'],
                 ],
         }
        ],

    50: [['defi-1', 'defi-2'],
         # pieces modèles 0 et modèle 1 respectivement pour le tas 0 et le tas 1
         {
            51: [['s-01'], # pieces similaires au modèle 0
                 ['s-11'], # pieces similaires au modèle 1
                 ],

            52: [['s-01', 's-02'],
                 ['s-11', 's-12'],
                ],

            53: [['s-01', 's-02', 's-03'],
                 ['s-11', 's-12', 's-13'],
                ],

            54: [['s-01', 's-02', 's-03', 's-04'],
                 ['s-11', 's-12', 's-13', 's-14'],
                 ],

            55: [['s-01', 's-02', 's-03', 's-04', 's-05'],
                 ['s-11', 's-12', 's-13', 's-14', 's-15'],
                 ],

            56: [['s-01', 's-02', 's-03', 's-04', 's-05', 's-06'],
                 ['s-11', 's-12', 's-13', 's-14', 's-15', 's-16']
                 ]
         }
        ],
}


audio_filenames = {
    '50':
    {
        'defi-1': 'N0-A',
        'defi-2': 'N0-B',

        's-01': 'N1-A5',
        's-11': 'N1-B5',

        's-02': 'N2-A10',
        's-12': 'N2-B10',

        's-03': 'N3-A11',
        's-13': 'N3-B3',

        's-04': 'N4-A8',
        's-14': 'N4-B8',

        's-05': 'N5-A3',

        's-06': 'N6-A7',
        's-15': 'N6-B7',

        's-07': 'N7-A1',
        's-16': 'N7-B1',
        's-17': 'N7-B11',
    }
}
