# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import logging
import logging.config
import datetime
import os

import composants
import sm130
import sm130wrapper

import utils
import pieces
import defs

import dictees
import similarites

CMD_ECOUTER = "CMD_ECOUTER"
CMD_VALIDER = "CMD_VALIDER"


def piece_ids_2_jeu_id(piece_ids):
    '''
    piece_ids : une liste d'identidiant de pieces
    retourne : le l'identifiant du jeu (dictee ou bien simila) dont une des pieces est l'ennonce
    '''
    jeu_id = None
    # est-ce une dictee ?
    dictee_pieces_ennoncees_deposees = list(set(piece_ids) & set(dictees.get_dictees_ennonce_piece_ids()))
    simila_pieces_ennoncees_deposees = list(set(piece_ids) & set(similarites.get_similarites_ennonce_piece_ids()))
    if len(dictee_pieces_ennoncees_deposees) == 1:
        piece_id = list(dictee_pieces_ennoncees_deposees)[0]
        jeu_id = dictees.ennonce_piece_ids_2_dictee_id(piece_id)
    # est-ce une similarite ?
    elif len(simila_pieces_ennoncees_deposees) > 0:
        jeu_id = similarites.ennonce_piece_ids_2_similarite_id(simila_pieces_ennoncees_deposees)
    return jeu_id


def jeu_id_is_dictee(jeu_id):
    return jeu_id in dictees.get_dictees_ids()

def jeu_id_is_similarite(jeu_id):
    return jeu_id in similarites.get_similarites_ids()

def init_tapis(addr):
    return  sm130wrapper.initPlateau(addr)

def attendre_piece_ennonce():
    zones = [[4*l+c+1 for c in range(0, 4)] for l in range(0, 4)]
    unJeu = Jeu(zones=zones)
    j_id = None
    while j_id is None:
        logging.getLogger("user").info("Attente de piece(s) ennonce(s)")
        _commande = unJeu.get_commande()
        etat = unJeu.get_etat_du_tapis()
        all_pieces_id = []
        for (_, t) in etat:
            if t is not None:
                all_pieces_id.append(t)
        j_id = piece_ids_2_jeu_id(all_pieces_id)
        # logging.getLogger("user").info("   all_pieces_id = %s, j_id = %s" % (all_pieces_id, j_id))
        if j_id is None:
            unJeu.jouer_attention()
    return j_id

# ==============================================================================

class Jeu(object):
    def __init__(self, zones):
        logging.getLogger().debug("Creation du jeu")
        pieces.init()

        self._zones = zones

        self.identifiant = datetime.datetime.now().isoformat()

        self._bouton = composants.Bouton()
        self._switch = composants.Switch()

        self._nb_colonnes = len(self._zones[0])
        self._nb_antennes = 4

        self._antennes_de_zone = {}
        self.__init_zones()

        self._etatTapis = None

        logging.getLogger().log(25, "%s;debut" % (self.identifiant))

    def __del__(self):
        logging.getLogger().log(25, "%s;fin" % (self.identifiant))

    def __init_zones(self):
        for i_col in range(self._nb_colonnes):
            for i_ant in range(self._nb_antennes):
                num_zone = self._zones[i_ant][i_col]
                if num_zone != 0:
                    if num_zone not in self._antennes_de_zone.keys():
                        self._antennes_de_zone[num_zone] = []
                    self._antennes_de_zone[num_zone].append((i_col, i_ant))

    def get_commande(self):
        logging.getLogger().debug("get_commande()")
        while not self._bouton.relache():
            logging.getLogger().log(9, "j'attend _bouton.relache()...")
        commande = CMD_ECOUTER
        logging.getLogger().log(25, "%s;Bouton;%s" % (self.identifiant, commande))
        return commande

    def get_etat_du_tapis(self):
        #~ [   ((x, y), id_piece),  ((x2, y2), id_piece2) ...  ]  # ((i_col, i_ant), id_piece)
        logging.getLogger().debug("get_etat_du_tapis...")
        import time
        t1 = time.clock()
        self._etatTapis = sm130wrapper.getEtatTapis()
        logging.getLogger().debug("get_etat_du_tapis : temps = %s" % (time.clock()  - t1))

        etats = []

        for i_col in range(self._nb_colonnes):
            self._switch.selectionner_colonne(i_col)
            for i_ant in range(self._nb_antennes):
                logging.getLogger().debug("   (i_col, i_ant)=(%s, %s)" % (i_col, i_ant))
                tag_id = self.get_tag_on_antenne(i_ant)
                logging.getLogger().debug("        tag_id=%s" % tag_id)
                piece_id = pieces.tag_id_2_piece_id(tag_id)
                logging.getLogger().debug("        piece_id=%s" % piece_id)
                etats.append(((i_col, i_ant), piece_id))
        logging.getLogger().debug("etats = %s..." % (etats))
        return etats

    def get_piece_id_by_zone(self):
        '''
        retourne un dict pid_by_z: pour chaque numero de zone, associe la liste des piece_id de la zone
        '''
        logging.getLogger().debug("recuperons la liste des tags par zone...")
        etats = self.get_etat_du_tapis()
        pid_by_z = {}
        for num_zone in self._antennes_de_zone:
            pid_by_z[num_zone] = []
            for (i_col, i_ant) in self._antennes_de_zone[num_zone]:
                for ((col_i_e, ant_i_e), piece_id) in etats:
                    if (col_i_e, ant_i_e) == (i_col, i_ant):
                        logging.getLogger().debug(" num_zone=%2s :  (i_col, i_ant), piece_id=(%s, %s), piece_id=%s" %
                                                  (num_zone, i_col, i_ant, piece_id)
                                                 )
                        pid_by_z[num_zone].append(piece_id)

        logging.getLogger().debug("pid_by_z=%s" % pid_by_z)
        return pid_by_z

    def verif_proposition(self, proposition):
        return None

    def a_gagne(self, proposition):
        return None

    def get_tag_on_antenne(self, i_ant):
        tag = None
        self._switch.allumer_antenne(i_ant)
        try:
            for ((c, a), t) in self._etatTapis:
                if (c, a) == (self._switch.current_colonne, self._switch.current_antenne):
                    tag = t
                    break
        except sm130.NoTagError:
            logging.getLogger().debug("antenne = %s -> NO_TAG_ERROR" % i_ant)
        return tag

    # def get_tag_en_ecoute(self):
    #     self._switch.selectionner_colonne(1)
    #     return self.get_tag_on_antenne(0)

    # def _colonne_utile(self, i_col):
    #     for ligne in self._zones:
    #         if ligne[i_col] != 0:
    #             return True
    #     return False

    def jouer_piece(self, piece_id, jeu_id=None, audio_filenames=None, wait_for_the_end=True):
        if piece_id is not None:
            piece_id_audio_filename = pieces.get_audio_filename(piece_id, jeu_id, audio_filenames)
            if not utils.music.play(piece_id_audio_filename, wait_for_the_end=wait_for_the_end):
                logging.getLogger().error("ERREUR : piece '%s' : probleme fichier audio" % piece_id_audio_filename)
        else:
            logging.getLogger().error("ERREUR : piece '%s' inconnu" % piece_id)

    def jouer_gagner(self):
        utils.music.play(os.path.join(os.path.sep, defs.AUDIO_ROOT_PATH, "gagne.ogg"), wait_for_the_end=True)

    def jouer_perdu(self):
        utils.music.play(os.path.join(os.path.sep, defs.AUDIO_ROOT_PATH, "perdu.ogg"), wait_for_the_end=True)

    # see package sound-theme-freedesktop
    def jouer_correct(self):
        utils.music.play("/usr/share/sounds/freedesktop/stereo/complete.oga", wait_for_the_end=True)

    def jouer_debutPartie(self):
        utils.music.play("/usr/share/sounds/freedesktop/stereo/service-login.oga", wait_for_the_end=True)

    def jouer_attention(self):
        utils.music.play("/usr/share/sounds/freedesktop/stereo/dialog-error.oga", wait_for_the_end=True)

# ==============================================================================

class JeuDeTri(Jeu):
    def __init__(self, zones, simi_id):
        import copy
        logging.getLogger().debug("Creation du jeu : JeuDeTri")
        Jeu.__init__(self, zones)

        self.simi_id = simi_id
        self.ennonce_piece_id = copy.deepcopy(similarites.get_piece_id_ennonce(simi_id))
        self.solutions = copy.deepcopy(similarites.get_piece_id_solutions(simi_id))

        logging.getLogger().debug("JeuDeTri : (ennonce, solutions)=(%s, %s)" % (self.ennonce_piece_id, self.solutions))

        for sol in self.solutions:
            for i in range(0, len(self.solutions[sol])):
                self.solutions[sol][i] = [self.ennonce_piece_id[i]]+self.solutions[sol][i][:]

        logging.getLogger().log(25, "%s;debut;%s" % (self.identifiant, self.simi_id))

    def a_gagne(self, proposition):
        gagne = None
        verif = self.verif_proposition(proposition)
        if verif['prop_is_solution'] is not None:
            logging.getLogger().log(25, "%s;gagne" % (self.identifiant))
            gagne = 1
        elif verif['prop_included_solution'] is not None:
            logging.getLogger().log(25, "%s;correct" % (self.identifiant))
            gagne = 0
        else:
            logging.getLogger().log(25, "%s;perdu" % (self.identifiant))
            gagne = -1
        return gagne


    def _piece_id_by_zone_2_proposition(self, pz):
        '''
        param : pz est un dict : pour chaque zone i, pz[i] contient la liste de toutes les pieces (ou bien absence d'une piece, indique avec None) de la zone i
        retourne : proposition, une list de list de : liste de piece_id (pour chaque zone num_zone, proposition[num_zone] contient la liste des pieces de la zone
        '''
        proposition = []
        for num_zone in self._antennes_de_zone:
            piece_id_list = pz[num_zone]
            pl = []
            for piece_id in piece_id_list:
                if piece_id is not None:
                    pl.append(piece_id)
            proposition.append(pl)
        return proposition

    def get_proposition(self):
        '''
        retourne : une list de list de piece_id
        '''
        logging.getLogger().debug("verification...")
        pz = self.get_piece_id_by_zone()
        return self._piece_id_by_zone_2_proposition(pz)

    def verif_proposition(self, proposition):
        '''
        proposition : uplet de liste de piece_id
        retourne vrai ssi proposition est une des solutions
        '''

        prop_is_solution = None
        prop_included_solution_list = []

        if sum([len(p) for p in proposition]) > 0: # la proposition ne doit pas etre vide
            prop_set = []
            for tas in proposition:
                prop_set.append(set(tas))

            for sol_id in self.solutions:
                sol_set = []
                for tas in self.solutions[sol_id]:
                    sol_set.append(set(tas))

                #logging.getLogger().debug("  proposition = %s - solution = %s (=> %s vs %s)" % (proposition, self.solutions, prop_set, sol_set))
                if (prop_set[0] == sol_set[0] and prop_set[1] == sol_set[1]) or (prop_set[0] == sol_set[1] and prop_set[1] == sol_set[0]):
                    prop_is_solution = sol_id
                elif (prop_set[0].issubset(sol_set[0]) and prop_set[1].issubset(sol_set[1])) or (prop_set[0].issubset(sol_set[1]) and prop_set[1].issubset(sol_set[0])):
                    prop_included_solution_list.append(sol_id)

        reponse = {
            "prop_is_solution": prop_is_solution,
            "prop_included_solution": prop_included_solution_list if len(prop_included_solution_list) > 0 else None
        }
        logging.getLogger().log(25, "%s;VERIF;%s;%s" % (self.identifiant, proposition, reponse))

        return reponse

    def jouer_proposition(self, proposition):
        '''
        proposition : liste de piece_id
        '''
        logging.getLogger().debug("Playing proposition %s..." % proposition)
        i = 0
        for piece_id_list in proposition:
            logging.getLogger().debug("   Tas %2s - piece_id_liste = %s" % (i, piece_id_list))
            logging.getLogger("user").info("   Tas %2s..." % (i))
            for piece_id in piece_id_list:
                logging.getLogger("user").info("      piece %2s..." % piece_id)
                if piece_id is not None:
                    self.jouer_piece(piece_id, jeu_id=self.simi_id, audio_filenames=similarites.audio_filenames, wait_for_the_end=True)
            i += 1

# ==============================================================================

class JeuDOrdre(Jeu):
    def __init__(self, zones, dictee_id):
        logging.getLogger().debug("Creation du jeu")
        Jeu.__init__(self, zones)

        self.ennonce_piece_id = dictees.get_piece_id_ennonce(dictee_id)
        self.solutions = dictees.get_piece_id_solutions(dictee_id)

        logging.getLogger().debug("JeuDOrdre : (ennonce, solutions)=(%s, %s)" % (self.ennonce_piece_id, self.solutions))

        self.dictee_id = dictee_id

        logging.getLogger().log(25, "%s;debut;%s" % (self.identifiant, self.dictee_id))

    def a_gagne(self, proposition):
        gagne = self.verif_proposition(proposition)
        if gagne:
            logging.getLogger().log(25, "%s;gagne" % (self.identifiant))
        else:
            logging.getLogger().log(25, "%s;perdu" % (self.identifiant))
        return gagne

    def _piece_id_by_zone_2_proposition(self, pz):
        '''
        param : pz est un dict : pour chaque zone i, pz[i] contient la liste des pieces de la zone i

        '''
        proposition = []
        for num_zone in self._antennes_de_zone:
            i = 0
            piece_id_list = pz[num_zone]
            while i < len(piece_id_list):
                piece_id = piece_id_list[i]
                if piece_id is not None:
                    proposition.append(piece_id)
                i += 1
        #~ logging.getLogger().debug("%s->%s" % (pz, proposition))
        return proposition

    def get_proposition(self):
        '''
        retourne une liste de piece_id
        '''
        logging.getLogger().debug("get_proposition...")
        pz = self.get_piece_id_by_zone()
        return self._piece_id_by_zone_2_proposition(pz)

    def verif_proposition(self, proposition):
        '''
        proposition : liste de piece_id
        retourne vrai ssi proposition est une des solutions
        '''
        logging.getLogger().debug("verif_proposition(self, proposition= %s)" % proposition)
        gagne = False
        num_sol = 0
        while num_sol < len(self.solutions) and not gagne:
            sol = self.solutions[num_sol]
            logging.getLogger().debug(" - sol = %s" % sol)
            gagne = (proposition == sol)
            num_sol += 1
        logging.getLogger().log(25, "%s;VERIF;%s;%s" % (self.identifiant, proposition, gagne))
        return gagne

    def jouer_proposition(self, proposition):
        '''
        proposition : liste de piece_id
        '''
        logging.getLogger().debug("Playing proposition %s..." % proposition)
        i = 0
        for piece_id in proposition:
            logging.getLogger().debug("piece_id=%s" % (piece_id))
            logging.getLogger("user").info("Piece %s..." % (i+1))
            self.jouer_piece(piece_id, jeu_id=None, audio_filenames=dictees.audio_filenames, wait_for_the_end=True)
            i += 1

    def jouer_morceau(self, wait_for_the_end=True):
        logging.getLogger().debug("jouer_morceau...")
        utils.music.play(pieces.get_audio_filename(self.ennonce_piece_id), wait_for_the_end=wait_for_the_end)
