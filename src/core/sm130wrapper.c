// This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
// Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
// Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
//               Emmanuel Leguy, Florence Levé, Hervé Midavaine

// "Les pierres musicales" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// "Les pierres musicales" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

#include <Python.h> //see package python-dev
#include "rfid.h"


// see https://docs.python.org/2.7/extending/building.html#building

int STATUS = -1;

static PyObject *m130wrapper_initPlateau(PyObject *self, PyObject *args)
{
   int addr = - 1;
   if (!PyArg_ParseTuple(args, "i", &addr))
   {
      STATUS = -2;
   }
   else
   {
      STATUS = initPlateau(addr);
   }
   return Py_BuildValue("i", STATUS);
}


static PyObject *sm130wrapper_getEtatTapis(PyObject *self) {
   PyObject *tup = PyList_New(NUMER_OF_LINE*NUMER_OF_COLUMN);

   unsigned int **tag_tab = fillEtatTapis();
   int l,c;
   int i = 0;
   char tag_str[TAG_LEN+1];

   if(STATUS >= 0)
   {
        for(c=0; c<NUMER_OF_COLUMN; c++)
        {
            for(l=0; l<NUMER_OF_LINE; l++)
            {
                void* val = NULL;
                if(tag_tab[l][c] > 0)
                {
                    if(tag_tab[l][c]< 0x10000000)
                        sprintf(tag_str, "0%x", tag_tab[l][c]);
                    else
                        sprintf(tag_str, "%x", tag_tab[l][c]);
                    val = tag_str;
                }
                PyList_SetItem(tup, i, Py_BuildValue("((ii)s)", c, l, val));
                i+=1;
            }
        }
   }
   return tup;
}

static PyObject *m130wrapper_attendreBouton(PyObject *self) {
    if (STATUS >= 0)
        attendreBouton();
    Py_RETURN_NONE;
}


static PyMethodDef sm130wrapper_funcs[] = {
   { "initPlateau", (PyCFunction)m130wrapper_initPlateau, METH_VARARGS, NULL },
   { "getEtatTapis", (PyCFunction)sm130wrapper_getEtatTapis, METH_NOARGS, NULL },
   { "attendreBouton", (PyCFunction)m130wrapper_attendreBouton, METH_NOARGS, NULL },
    {NULL}
};

void initsm130wrapper(void)
{
    Py_InitModule3("sm130wrapper", sm130wrapper_funcs, NULL);
}
