# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>


import defs

similarities = {}
audio_filenames = {}

for similarites_module in defs.list_similarites:
    try:
        similarities_data = __import__(similarites_module)
        similarities.update(similarities_data.similarites 
                            if similarities_data.similarites is not None else {})
        audio_filenames.update(similarities_data.audio_filenames
                               if similarities_data.audio_filenames is not None else {})
    except ImportError:
        print("WARNING: pas de module %s" % similarites_module)

def get_similarites_ids():
    return similarities.keys()


def get_piece_id_ennonce(sim_id):
    if sim_id in similarities:
        return similarities[sim_id][0]
    else:
        return None


def get_piece_id_solutions(sim_id):
    if sim_id in similarities:
        return similarities[sim_id][1]
    else:
        return None

# ======================================================================
def get_similarites_ennonce_piece_ids():
    '''
    retourne la liste des piece id des enonnces
    '''
    similarites_ids = []
    for s in similarities:
        for p_id in get_piece_id_ennonce(s):
            similarites_ids.append(p_id)
    return similarites_ids

def ennonce_piece_ids_2_similarite_id(ennonce_piece_ids):
    for sim_id in similarities:
        sim_ennonce_piece_id = get_piece_id_ennonce(sim_id)
        if  set(sim_ennonce_piece_id) == set(ennonce_piece_ids):
            return sim_id
    return None

# ======================================================================
if __name__ == '__main__':
    import argparse
    import sys

    import utils
    import pieces

    parser = argparse.ArgumentParser(description='Verifier les jeux')
    parser.add_argument('-s', '--sound',
                        action='store_true',
                        help='with sound?',
                        default=False)
    parser.add_argument('-i', '--idjeu',
                        type=int,
                        help='id du jeu')
    args = parser.parse_args()

    pieces.init()

    if args.idjeu:
        simi_id_list = [args.idjeu]
    else:
        simi_id_list = similarities.keys()
        simi_id_list.sort()
    for simi_id in simi_id_list:
        simi_ennonce = get_piece_id_ennonce(simi_id)
        simi_soluces = get_piece_id_solutions(simi_id)
        print "* simi_id='%s' : simi_ennonce='%s' ->"  % (simi_id, simi_ennonce)
        if args.sound:
            for ennonce_piece_id in simi_ennonce:
                ennonce_piece_id_audiofile_name = pieces.get_audio_filename(ennonce_piece_id, simi_id, audio_filenames)
                if not utils.music.play(ennonce_piece_id_audiofile_name, wait_for_the_end=True):
                    print "Erreur de fichier audio : %s" % ennonce_piece_id_audiofile_name

        for soluces_id in simi_soluces:
            print "  - soluces_id=%s " % (soluces_id)
            for s_ids in simi_soluces[soluces_id]:
                print "    - %s " % (s_ids)

                if args.sound:
                    for piece_id in s_ids:
                        piece_id_audio_filename = pieces.get_audio_filename(piece_id, simi_id, audio_filenames)
                        if not utils.music.play(piece_id_audio_filename, wait_for_the_end=True):
                            print "Erreur de fichier audio: %s" % piece_id_audio_filename

    print "Nombre de similarities : %d " % len(simi_id_list)
    sys.exit(0)
