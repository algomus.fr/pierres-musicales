// This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
// Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
// Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
//               Emmanuel Leguy, Florence Levé, Hervé Midavaine

// "Les pierres musicales" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// "Les pierres musicales" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

#ifndef RFID_PCDUINO_H
#define RFID_PCDUINO_H

//  col_S1 col_S0 : balayage des 4 colonnes
//  line_s1 line_s0 : balayage des lignes d'une colonne
//  /e3 /e2 /e1 /e0  : choix de la colonne à balayer - actif à l'état bas

#define col_s0 7 //S0
#define col_s1 6 //S1

#define line_s0 5 //s0
#define line_s1 4 //s1

#define e0 3
#define e1 2
#define e2 1
#define e3 0

#define BP_PIN 8

#define PIN_LED_RED 9
#define PIN_LED_GREEN 10
#define PIN_LED_YELOW 11

#endif
