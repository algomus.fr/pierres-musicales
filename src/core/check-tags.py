# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import defs
import sm130wrapper
import utils
import pieces

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Verifier les jeux')
    parser.add_argument('-s', '--sound',
                        action='store_true',
                        help='with sound?',
                        default=False)
    args = parser.parse_args()

    pieces.init()
    if sm130wrapper.initPlateau(defs.I2C_ADDR) < 0:
        print "ERROR: initPlateau"
        return -1

    num_tag = 1
    tags = []
    while True:
        etat = sm130wrapper.getEtatTapis()
        for ((i_col, i_ant), tag) in etat:
            #if True:
            if tag is not None and tag not in tags:
                # tags.append(tag)
                piece_id = pieces.tag_id_2_piece_id(tag)
                print "(i_col, i_ant, tag) = (%s, %s, %s) => piece_id = %s" % (i_col, i_ant, tag, piece_id)
                if piece_id is not None and args.sound:
                    utils.music.play(pieces.get_audio_filename(piece_id), wait_for_the_end=True)
                num_tag += 1
        utils.delay(10)

if __name__ == '__main__':
    main()
