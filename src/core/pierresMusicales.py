# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import logging

import defs
import jeu
import ordonner
import trier

if __name__ == '__main__':
    logging.config.fileConfig("pierresMusicales_log.conf", disable_existing_loggers=False)

    logging.getLogger("user").info("Lancement du jeu")
    logging.getLogger().info("Lancement du jeu")

    if jeu.init_tapis(defs.I2C_ADDR) < 0 :
        print("Erreur au Lancement du jeu")
    else:
        j_id = jeu.attendre_piece_ennonce()
        gagne = False
        while True:
            if jeu.jeu_id_is_dictee(j_id):
                (j_id, gagne) = ordonner.jouer(j_id)
            elif jeu.jeu_id_is_similarite(j_id):
                (j_id, gagne) = trier.jouer(j_id)
            else:
                j_id = j_id = jeu.attendre_piece_ennonce()

            if gagne:
                j_id = jeu.attendre_piece_ennonce()
