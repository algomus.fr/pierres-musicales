// This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
// Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
// Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
//               Emmanuel Leguy, Florence Levé, Hervé Midavaine

// "Les pierres musicales" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// "Les pierres musicales" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

#include <stdio.h>
#include <malloc.h>

#include "rfid.h"
#include "rfid-pcduino.h"

// rfid.c

// sur carte expérimental MIS - projet RFID/Tag
// aiguillage lignes et colonnes  tapis de 4x4
// 4 Mux/Démux pour 4 colonnes de 4 lignes - s1 s0 & /e0,/e1,/e2 et /e3 pour valider la colonne
// 1 Mux/Démux pour 4 colonnes - S1 S0 & /E
// test sur 4 colonnes de 4 lignes !

#define NUMER_OF_BYTES_MSG_WITH_TAG 7

unsigned char data[NUMER_OF_COLUMN][NUMER_OF_LINE*NUMER_OF_BYTES_MSG_WITH_TAG];  // data = totalité des données sur le protocole de lecture
unsigned char N_serie[NUMER_OF_COLUMN][NUMER_OF_LINE*NUMER_OF_BYTES_FOR_TAG];    // N_serie = N° de dérie du tag, si présent !
unsigned char tag [NUMER_OF_COLUMN][NUMER_OF_LINE];                              // tag present = 1 ou tag absent = 0
unsigned int ** tag_tab = NULL;

// ===========================================================================
// ===========================================================================
void debug_display_arrays(void);

// ===========================================================================
void create_tag_tab(void)
{
    int l;
    tag_tab = malloc(NUMER_OF_LINE * sizeof(unsigned int));
    for (l=0; l<NUMER_OF_LINE; l++)
    {
        tag_tab[l] = malloc(NUMER_OF_COLUMN * sizeof(unsigned int));
    }
}

void debug_display_tag_tab(void)
{
    int l, c;
    for (c=0;c<NUMER_OF_COLUMN;++c)
    {
        for (l=0;l<NUMER_OF_LINE; ++l)
        {
            printf("0x%8X ", tag_tab[l][c]);
        }
        printf("\n");
    }
}

void reset_tabs(void)
{
    int c, l;
    for (c=0;c<NUMER_OF_COLUMN;++c)
    {
        for (l=0; l<NUMER_OF_LINE*NUMER_OF_BYTES_MSG_WITH_TAG; ++l)
            data[c][l] = 0x00;
        for (l=0; l<NUMER_OF_LINE*NUMER_OF_BYTES_FOR_TAG; ++l)
            N_serie[c][l] = 0x00;
        for (l=0; l<NUMER_OF_LINE; ++l)
            tag[c][l] = 0;
    }
}

void debug_display_arrays(void)
{
    int c, l;
    printf("data:\n");
    for (c=0; c<NUMER_OF_COLUMN; ++c)
    {
            for (l=0; l<NUMER_OF_LINE*NUMER_OF_BYTES_MSG_WITH_TAG; ++l)
            {
                printf ("%2x ", data[c][l]);
                if ((l+1)%NUMER_OF_BYTES_MSG_WITH_TAG==0)
                 printf (" - ");
            }
            printf ("\n");
    }
    printf ("\n");

    printf("N_serie:\n");
    for (c=0;c<NUMER_OF_COLUMN;++c)
    {
            for (l=0;l<NUMER_OF_LINE*NUMER_OF_BYTES_FOR_TAG;++l)
            {
                printf ("%2x ", N_serie[c][l]);
                if ((l+1)%NUMER_OF_BYTES_FOR_TAG==0)
                 printf (" - ");
            }
            printf ("\n");
    }
    printf ("\n");

    printf("tag:\n");
    for (c=0;c<NUMER_OF_COLUMN;++c)
    {
            for (l=0;l<NUMER_OF_LINE;++l)
            {
                printf ("%2x ", tag[c][l]);
                if ((l+1)%NUMER_OF_LINE==0)
                   printf (" - ");
            }
            printf ("\n");
    }
    printf ("\n");
}

void N_serie_and_tag_2_tab(void)
{
    int i, c, l;
    unsigned int t = 0;

    DEBUG("BEGIN");

    for (c=0;c<NUMER_OF_COLUMN;++c)
        for (l=0;l<NUMER_OF_LINE; ++l)
        {
            if (tag[c][l] == 1)
            {
                for (i=0; i<NUMER_OF_BYTES_FOR_TAG; i++)
                {
                    t = (t << 2*4) + N_serie[c][l*NUMER_OF_BYTES_FOR_TAG+i];
                }
            }
            else
            {
                 t = 0;
            }
            tag_tab[l][c] = t;
        }
        DEBUG("END");
}


// ===========================================================================

void activer_colonne(int x)
{
    sprintf(DEBUG_STR, "x=%d", x);DEBUG(DEBUG_STR);

    if(x==0)
    {
        envoyerBit(col_s0,1); //   col_S0=1  colonne 0  !!! 2Y3 <=> 1Y3
        envoyerBit(col_s1,1); //   col_S1=1  colonne 0  !!! 2Y3 <=> 1Y3

        envoyerBit(e0,0);
        envoyerBit(e1,1);
        envoyerBit(e2,1);
        envoyerBit(e3,1);
    }
    else
    if(x==1)
    {
        envoyerBit(col_s0,0); //   S0=0  colonne 1  !!! 2Y2<=> 1Y2
        envoyerBit(col_s1,1); //   S1=1  colonne 1  !!! 2Y2 <=> 1Y2

        envoyerBit(e0,1);
        envoyerBit(e1,0);
        envoyerBit(e2,1);
        envoyerBit(e3,1);
    }
    else
    if(x==2)
    {
        envoyerBit(col_s0,1); //   S0=1  colonne 2  !!! 2Y1 <=> 1Y1
        envoyerBit(col_s1,0); //   S1=0  colonne 2  !!! 2Y1 <=> 1Y1

        envoyerBit(e0,1);
        envoyerBit(e1,1);
        envoyerBit(e2,0);
        envoyerBit(e3,1);
    }
    else
    if(x==3)
    {
        envoyerBit(col_s0,0); //   S0=0  colonne 3  !!! 2Y0 <=> 1Y0
        envoyerBit(col_s1,0); //   S1=0  colonne 3  !!! 2Y0 <=> 1Y0

        envoyerBit(e0,1);
        envoyerBit(e1,1);
        envoyerBit(e2,1);
        envoyerBit(e3,0);
    }
}

void activer_ligne(int x)
{
    sprintf(DEBUG_STR, "x=%d", x);DEBUG(DEBUG_STR);
    if(x==0)
    {
        envoyerBit(line_s0,0); //   s0=0
        envoyerBit(line_s1,0); //   s1=0
    }
    else
    if(x==1)
    {
        envoyerBit(line_s0,1); //   s0=1
        envoyerBit(line_s1,0); //   s1=0
    }
    else
    if(x==2)
    {
        envoyerBit(line_s0,0); //   s0=0
        envoyerBit(line_s1,1); //   s1=1
    }
    else
    if(x==3)
    {
        envoyerBit(line_s0,1); //   s0=1
        envoyerBit(line_s1,1); //   s1=1
    }
}

void balayage_plateau(void)
{
    DEBUG("");
    //**************************************
    //******** Balayage colonne ************
    //**************************************
    int c;
    reset_tabs();
    DEBUG("");
    for(c=0; c<NUMER_OF_COLUMN; c++)
    {
        balayage_lignes(c);
    }
}

void balayage_lignes(int i_col)
{
    ////////////////////
    // balayage lignes de la colonne c (qui sera activée dans la fonction)
    ////////////////////

    int i_ligne;
    // activation de la colonne
    activer_colonne(i_col);
    for(i_ligne=0; i_ligne<NUMER_OF_LINE; i_ligne++)
    {
		sprintf(DEBUG_STR, "i_ligne=%d", i_ligne);DEBUG(DEBUG_STR);

        activer_ligne(i_ligne);
        lecture(i_col, i_ligne);
        alumerEteindreLED(i_ligne);
    }
}

int lecture(int i_col, int i_ligne)
{
    unsigned char buf[256];
    int i, k;
    int l = i_ligne * NUMER_OF_BYTES_MSG_WITH_TAG;
    int m = i_ligne * NUMER_OF_BYTES_FOR_TAG;

    sprintf(DEBUG_STR, "int i_col=%d, int i_ligne=%d", i_col, i_ligne);DEBUG(DEBUG_STR);

    envoyerCommande(COMMAND_SELECT_TAG);
    attendre(15); // attente phase d'ecriture et de lecture - cf datasheet page 8 AN601.pdf

    if (lectureEtRemplissageBuffer(buf) < 0)
        return -1;

    sprintf(DEBUG_STR, "buf[0]=%x", buf[0]); DEBUG(DEBUG_STR);
    for (i=0; i <=buf[0]; i++)
    {
            sprintf(DEBUG_STR, "   buf[%d]=%x", i, buf[i]);DEBUG(DEBUG_STR);
    }

    data[i_col][0+l] = buf[0]; // lecture de length
    sprintf(DEBUG_STR, "data[%d][%d]=%x", i_col, 0+l,data[i_col][0+l]); DEBUG(DEBUG_STR);
    if (data[i_col][0+l] <= 2)
    {
        tag [i_col][i_ligne]=0;
        if (buf[2] == ErrorCode_No_Tag_present)
        {
            sprintf(DEBUG_STR, "i_col=%x: ErrorCode_No_Tag_present", i_col);    DEBUG(DEBUG_STR);
        }
        return -2;
    }

    // longueur = 6, tag present
    tag[i_col][i_ligne]=1;
    sprintf(DEBUG_STR, "tag[c][n]=%x", tag[i_col][i_ligne]);DEBUG(DEBUG_STR);

    data[i_col][1+l] = buf[1]; // rappel de la commande
    sprintf(DEBUG_STR, "data[%d][%d]=%x", i_col, 1+l,data[i_col][1+l]); DEBUG(DEBUG_STR);

    data[i_col][2+l] = buf[2]; // lecture du type de tag "standart"
    sprintf(DEBUG_STR, "data[%d][%d]=%x", i_col, 2+l, data[i_col][2+l]);DEBUG(DEBUG_STR);

    for(k=0; k<NUMER_OF_BYTES_FOR_TAG; k++)
    {
        sprintf(DEBUG_STR, "data[%d][%d]=%x", i_col, 3+l+k,data[i_col][3+l+k]);DEBUG(DEBUG_STR);
        if (tag[i_col][i_ligne] == 0) // pas de tag
        {
            //DEBUG("pas de tag");
            N_serie[i_col][m+k]=00;
        }
        else // tag present
        {
            data[i_col][3+l+k] = buf[3+k];
            N_serie[i_col][m+k]= data[i_col][3+l+k];
            sprintf(DEBUG_STR, "N_serie[%d][%d]=%x", i_col, m+k, N_serie[i_col][m+k]);DEBUG(DEBUG_STR);
        }
    }

    return 0;
}

// ===========================================================================
int initPlateau(int addr)
{
        sprintf(DEBUG_STR, "addr=0x%x", addr);DEBUG(DEBUG_STR);
        if(init(addr) == 0)
        {
            create_tag_tab();
            return 0;
        }
        else
        {
            fprintf(stderr, "void initPlateau(int addr): failed\n");
            return -1;
        }
}

unsigned int** fillEtatTapis(void)
{
        balayage_plateau();
        //debug_display_arrays();
        N_serie_and_tag_2_tab();
        //debug_display_tag_tab();
        return tag_tab;
}
