// This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
// Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
// Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
//               Emmanuel Leguy, Florence Levé, Hervé Midavaine

// "Les pierres musicales" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// "Les pierres musicales" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

// GPIO: based on the example code (beerware) for GPIO by Mike Hord https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/gpio/gpio_test.cpp

// I2C :
// https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/I2C/i2c.cpp
// https://www.kernel.org/doc/Documentation/i2c/dev-interface
//
// GPIO :
// https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/gpio/gpio_test.cpp
// http://digitalhacksblog.blogspot.fr/2013/06/pcduino-blink-tutorial.html

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#include "rfid.h"
#include "rfid-pcduino.h"

// see https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/gpio/gpio_test.cpp
#define GPIO_MODE_PATH "/sys/devices/virtual/misc/gpio/mode/gpio%d"
#define GPIO_PIN_PATH  "/sys/devices/virtual/misc/gpio/pin/gpio%d"

#define DEBUG_LECTURE_REMPLISSAGE 1

#define LOW "0"
#define HIGH "1"

#define MODE_INPUT  "0"
#define MODE_OUTPUT  "1"
#define MODE_INPUT_PU "8"

#define GPIO_NB  (8+3+1)
const int GPIO_PIN_NUM[GPIO_NB] = {col_s0, col_s1,
                                   line_s0, line_s1,
                                   e0, e1, e2, e3,
                                   PIN_LED_RED, PIN_LED_GREEN, PIN_LED_YELOW,
                                   BP_PIN
                                };
const char* GPIO_PIN_MODE[GPIO_NB] = {MODE_OUTPUT, MODE_OUTPUT,
                                      MODE_OUTPUT, MODE_OUTPUT,
                                      MODE_OUTPUT, MODE_OUTPUT,MODE_OUTPUT, MODE_OUTPUT,
                                      MODE_OUTPUT, MODE_OUTPUT,MODE_OUTPUT,
                                      MODE_INPUT_PU
                                };

int GPIO_MODE_FD[18];
int GPIO_PIN_FD[18];

const int BUF_SIZE = 32 ;
int I2C_FD = -1 ;

//============================================================================
int init_I2C(int address)
{
    int code_ret = 0;
    I2C_FD = open("/dev/i2c-2", O_RDWR);
    if (I2C_FD < 0)
    {
        perror("open /dev/i2c-2");
        code_ret = -1;
    }
    else
    {
        DEBUG("open:ok");
    }

    if (ioctl(I2C_FD, I2C_SLAVE, address) < 0) {
        perror("ioctl(I2C_FD, I2C_SLAVE, address)");
        code_ret = -2;
    }
    else
    {
        DEBUG("ioctl:ok");
    }
    return code_ret;
}


//============================================================================
// see https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/gpio/gpio_test.cpp
int init_gpio(void)
{
    int i;
    char s[255];
    char err_str[256];
    int code_ret = 0;

    DEBUG("BEGIN");
    for(i=0; i < GPIO_NB; i++)
    {
        int pin = GPIO_PIN_NUM[i];
        GPIO_MODE_FD[pin] = -1;
        GPIO_PIN_FD[pin] = -1;
    }

    for(i=0; i < GPIO_NB; i++)
    {
        int pin = GPIO_PIN_NUM[i];
        sprintf(DEBUG_STR, "i=%d pin=%d", i, pin); DEBUG(DEBUG_STR);

        sprintf(s, GPIO_MODE_PATH, pin);
        sprintf(DEBUG_STR, "  s=%s", s); DEBUG(DEBUG_STR);
        GPIO_MODE_FD[pin] = open(s, O_RDWR);
        if (GPIO_MODE_FD[pin] < 0)
        {
            sprintf(err_str, "ERROR: int init_gpio(void): %s", s);
            perror(err_str);
            code_ret = -2;
            break;
        }

        sprintf(s, GPIO_PIN_PATH, pin);
        sprintf(DEBUG_STR, "  s=%s", s); DEBUG(DEBUG_STR);
        GPIO_PIN_FD[pin] = open(s, O_RDWR);
        if (GPIO_PIN_FD[pin] < 0)
        {
            sprintf(err_str, "ERROR: int init_gpio(void): %s", s);
            perror(err_str);
            code_ret = -1;
            break;
        }

        sprintf(DEBUG_STR, "pin=%d GPIO_PIN_MODE[i]=%s", pin, GPIO_PIN_MODE[i]); DEBUG(DEBUG_STR);
        if(write(GPIO_MODE_FD[pin], GPIO_PIN_MODE[i], 1) < 0 )
        {
            sprintf(err_str, "i=%d; write(GPIO_MODE_FD[i], %s, 1)", i, GPIO_PIN_MODE[i]);
            perror(err_str);
            code_ret = -3;
            break;
        }
    }
    DEBUG("END");
    return code_ret;
}

int init(int address)
{
    int code_ret = 0;
    if (init_gpio() < 0)
    {
        code_ret = -1 ;
    }
    else
    {
        if(init_I2C(address) < 0)
        {
            fprintf(stderr, "init_I2C(address): error\n");
            code_ret = -2 ;
        }
        else
        {
            unsigned char buf[256];
            if (envoyerCommande(COMMAND_FIRMWARE) < 0)
            {
                fprintf(stderr, "EnvoyerCommande(COMMAND_FIRMWARE): error\n");
                code_ret = -3 ;
            }
            else
            {
                if(lectureEtRemplissageBuffer(buf)<0)
                {
                    fprintf(stderr, "lectureEtRemplissageBuffer(buf): error\n");
                    code_ret = -3 ;
                }
            }
        }
    }
    return code_ret;
}

//============================================================================
void attendre(int d)
{
    DEBUG("usleep...");
    usleep(d*1000);
}

void alumerEteindreLED(int ligne)
{
    const int duree = 1;

    int pin;
    if(0 <= ligne && ligne <= 2)
    {
        if(ligne == 0)
            pin = PIN_LED_RED;
        else
        if(ligne == 1)
            pin = PIN_LED_GREEN;
        else
            pin = PIN_LED_YELOW;
        envoyerBit(pin, 1);
        attendre(duree);
        envoyerBit(pin, 0);
    }
    else
    {
        for(pin = PIN_LED_RED; pin <= PIN_LED_YELOW; pin+=1)
            envoyerBit(pin, 1);
        attendre(duree);
        for(pin = PIN_LED_RED; pin <= PIN_LED_YELOW; pin+=1)
            envoyerBit(pin, 0);
    }
}

void envoyerBit(int pin, int b)
{
    int f;
    char * s;

    sprintf(DEBUG_STR, "pin=%d - b=%d", pin, b); DEBUG(DEBUG_STR);

    f = GPIO_PIN_FD[pin];
    if(f < 0)
    {
        fprintf(stderr, "void envoyerBit(int pin=%d, int b=%d): pin non itialisée\n", pin, b);
    }
    else
    {
        if (b == 0)
            s = LOW;
        else
            s = HIGH;
        lseek(f, 0, SEEK_SET);   // see https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/gpio/gpio_test.cpp
        if(write(f, s, 1) < 0)
        {
            char msg_err[256];
            sprintf(msg_err, "void envoyerBit(int pin, int b): write(f=%d, s=%s, 1)", f, s);
            perror(msg_err);
        }
    }
}

unsigned char lireBits(int pin)
{
    int f;
    char inputBuffer;

    sprintf(DEBUG_STR, "pin=%d", pin); DEBUG(DEBUG_STR);
    f = GPIO_PIN_FD[pin];
    if(read(f, &inputBuffer, 1) < 0)
    {
        char msg_err[256];
        sprintf(msg_err, "unsigned char lireBits(int pin=%d): read(f, &inputBuffer, 1)", pin);
        perror(msg_err);
    }
    sprintf(DEBUG_STR, "inputBuffer=%d ('%c')", inputBuffer, inputBuffer); DEBUG(DEBUG_STR);
    return inputBuffer;
}

void attendreBouton()
{
  printf("Attente du bouton...\n");
  char inputBuffer = '1';
  do
  {
    // see https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/gpio/gpio_test.cpp
    lseek(GPIO_PIN_FD[BP_PIN], 0, SEEK_SET);
    inputBuffer = lireBits(BP_PIN);
    attendre(10);
  } while (inputBuffer == '1');

}


int envoyerCommande(unsigned char commande)
{
  int code_ret = 0;
  unsigned char buf[BUF_SIZE];

  DEBUG("envoyerCommande: BEGIN");

  buf[0] = 0x01;
  buf[1] = commande;
  buf[2] = (buf[0]+buf[1])%0xff;

#ifdef DEBUG_ENVOYER
  DEBUG("  envoie...");
  for (i=0; i<=2; i++)
  {
    sprintf(DEBUG_STR, "  buf[%d]=0x%x", i, buf[i]);
    DEBUG(DEBUG_STR);
  }
#endif //DEBUG_ENVOYER

  if(I2C_FD < 0)
  {
      fprintf(stderr, "int envoyerCommande(unsigned char commande = %x): I2C non initialisé\n", commande);
      code_ret = -2;
  }
  else
  if (write(I2C_FD, buf, 3) != 3)
  {
        char msg_err[256];
        sprintf(msg_err, "int envoyerCommande(unsigned char commande = %x): write:", commande);
        perror(msg_err);
        code_ret = -1;
  }
  else
  {
      DEBUG("write:ok");
  }
  DEBUG("envoyerCommande: END");
  return code_ret;
}

int lectureEtRemplissageBuffer(unsigned char* buf)
{
  int code_ret = 0;
  int i;
  unsigned char nb_byte = 0;
  DEBUG("lectureEtRemplissageBuffer: BEGIN");
  for (i=0; i<BUF_SIZE; i++)
  {
      buf[i] = 255;
  }

  if(I2C_FD < 0)
  {
      fprintf(stderr, "int lectureEtRemplissageBuffer(unsigned char* buf): I2C non initialisé\n");
      code_ret = -2;
  }
  else
  if (read(I2C_FD, buf, BUF_SIZE) != BUF_SIZE)
  {
        char msg_err[256];
        sprintf(msg_err, "int lectureEtRemplissageBuffer(unsigned char* buf): read:");
        perror(msg_err);
        code_ret = -1;
  }
  else
  {
    DEBUG("read: ok");
  }

#if DEBUG_LECTURE_REMPLISSAGE > 0
  // lg
  nb_byte = buf[0];

  sprintf(DEBUG_STR, "buf[0]: nb_byte=%d", nb_byte);
  DEBUG(DEBUG_STR);

  // cmd
  sprintf(DEBUG_STR,"buf[1]: commande: buf[%d]=0x%x", 1, buf[1]);
  DEBUG(DEBUG_STR);

  // reponse
  for (i=2; i<=nb_byte && i<BUF_SIZE ; i++)
  {
    sprintf(DEBUG_STR,"  buf[%d]=0x%x(%c)", i, buf[i], buf[i]);
    DEBUG(DEBUG_STR);
  }
  // checksum
  sprintf(DEBUG_STR,"checksum: buf[%d]=0x%x", nb_byte+1, buf[nb_byte+1]);
  DEBUG(DEBUG_STR);
#endif

  DEBUG("lectureEtRemplissageBuffer: END");
  return code_ret;
}
