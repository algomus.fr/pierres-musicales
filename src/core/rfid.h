// This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
// Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
// Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
//               Emmanuel Leguy, Florence Levé, Hervé Midavaine

// "Les pierres musicales" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// "Les pierres musicales" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

#ifndef RFID_H
#define RFID_H

#include <stdio.h>

#define NUMER_OF_COLUMN 4
#define NUMER_OF_LINE 4

void debug_display_tag_tab(void);

char DEBUG_STR[256];
//#define DEBUG(x) printf("DEBUG [%20s] [%30s] [%4d] : %s\n", __FILE__, __FUNCTION__, __LINE__, x)
#define DEBUG(x) ;


#define NUMER_OF_BYTES_FOR_TAG 4
#define TAG_LEN  (2*NUMER_OF_BYTES_FOR_TAG)


#define COMMAND_RESET       0x80
#define COMMAND_FIRMWARE    0x81
#define COMMAND_SELECT_TAG  0x83

#define ErrorCode_No_Tag_present 0x4E

int initPlateau(int addr);
unsigned int** fillEtatTapis(void);
// à surcharger sur le pcduino et micro-controleur
void attendreBouton(void);

int lecture(int c, int i_ligne);
void balayage_lignes(int c);

// à surcharger sur le pcduino et micro-controleur
void attendre(int d);
int init(int addr);
void envoyerBit(int pin, int b);
int envoyerCommande(unsigned char commande);
int lectureEtRemplissageBuffer(unsigned char* buf);
void alumerEteindreLED(int i);

#endif
