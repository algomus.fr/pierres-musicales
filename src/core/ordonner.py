# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import logging
import logging.config

import defs
import jeu

zones = [
          [  4,  3,  2,  1],
          [  8,  7,  6,  5],
          [ 12,  9,  8,  7],
          [ 16, 15, 14, 13]
        ]

def jouer(dictee_id):
    logging.getLogger("user").info("*Nouvelle dictee...")
    partie = jeu.JeuDOrdre(zones, dictee_id)

    logging.getLogger("user").info("Ecoute la dictee...")
    partie.jouer_piece(partie.ennonce_piece_id, wait_for_the_end=True)

    commande = None
    partieTerminee = False
    partieGagnee = False
    while not partieTerminee:
        logging.getLogger().debug("Attente de commande....")
        logging.getLogger("user").info("Depose tes pieces et appuie sur le bouton")
        commande = partie.get_commande()
        logging.getLogger().debug("Commande = %s" % commande)
        proposition = partie.get_proposition()
        logging.getLogger("user").info("  Proposition = %s " % proposition)
        if len(proposition) < 1:
            logging.getLogger("user").info("Pas de piece posee ou detectees")
        elif commande == jeu.CMD_ECOUTER:
            nouveau_jeu_id = jeu.piece_ids_2_jeu_id(proposition)
            if nouveau_jeu_id is not None:
                logging.getLogger().debug("piece ennonce detectee : nouveau_jeu_id=%s"% (nouveau_jeu_id))
                if jeu.jeu_id_is_dictee(nouveau_jeu_id) :
                    if nouveau_jeu_id != dictee_id:
                        logging.getLogger("user").info("Nouveau jeu dictee detecte...")
                        dictee_id = nouveau_jeu_id
                        partieTerminee = True
                    else:
                        logging.getLogger("user").info("Ecoute la dictee...")
                        partie.jouer_piece(partie.ennonce_piece_id, wait_for_the_end=False)
                else:
                    logging.getLogger("user").info("Nouveau type de jeu detecte...")
                    dictee_id = nouveau_jeu_id
                    partieTerminee = True
            else:
                logging.getLogger("user").info("Ecoute les %s piece(s) de la proposition..." % len(proposition))
                partie.jouer_proposition(proposition)
                if len(proposition) == len(partie.solutions[0]):
                    logging.getLogger("user").info("Verifions...")
                    if partie.a_gagne(proposition):
                        logging.getLogger("user").info("********************** BRAVO! ***********************")
                        partie.jouer_gagner()
                        partieTerminee = True
                        partieGagnee = True
                    else:
                        partie.jouer_perdu()
                        logging.getLogger("user").info("********************** ESSAIE ENCORE... *************")
    #end while

    del partie
    return (dictee_id, partieGagnee)
