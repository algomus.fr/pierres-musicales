# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

# Pour chaque niveau, il y a l'énoncé, puis la solution ou les solutions possibles

dictees_melodie = {
    # Frère Jacques
    12: ('fj', [['fj-1', 'fj-2', 'fj-3', 'fj-4']]),

    # Sur le pont d'Avignon (attention, deux solutions !)
    13: ('pa', [['pa-1', 'pa-2', 'pa-3', 'pa-4'],
                ['pa-3', 'pa-2', 'pa-1', 'pa-4']]),

    # À la claire fontaine
    14: ('cf', [['cf-1', 'cf-2', 'cf-3', 'cf-4']]),

    # Boléro
    15: ('bo', [['bo-1', 'bo-2', 'bo-3', 'bo-4']])
}

dictees_notes = {
    ### Dictées de notes (niveaux 21 à 23)
    21: ('d-1', [['060', '064', '062', '067']]),  # c e d g
    22: ('d-2', [['067', '071', '074', '072']]),  # g b d' c'
    23: ('d-3', [['072', '060', '067', '064']]),  # c' c g e

    # plus grave
    # 24: ('defi-2', [['067', '071', '072', '074']]),  #
    # 25: ('defi-1', [['060', '062', '064', '067']]),  # aigu -> grave
    26: ('defi-3', [['072', '067', '064', '060']]),  # aigu -> grave
}

audio_filenames = None
