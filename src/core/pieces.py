# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import os
import logging

import defs

pieces = {}
notes = []
for pieces_module in defs.list_pieces:
    try:
        pieces_data = __import__(pieces_module)
        pieces.update(pieces_data.pieces
                               if pieces_data.pieces is not None else {})
        notes += pieces_data.notes if pieces_data.notes is not None else []
    except ImportError:
        print("WARNING: pas de module %s" % pieces_module)




def init():
    for piece_id in pieces:
        pieces[piece_id] = [ [ t.lower() for t in lt] for lt in  pieces[piece_id]]

# Chaque piece a un fichier ogg associé, comme "piece-060.ogg"
def get_audio_filename(piece_id, jeu_id=None, audio_filenames=None):
    f = "piece-%s.ogg" % piece_id

    rep_id = '' if jeu_id is None else str(jeu_id)

    if audio_filenames is not None:
        try:
            f = "%s.ogg" % audio_filenames[rep_id][piece_id]
        except KeyError:
            pass
    return os.path.join(os.path.sep, defs.AUDIO_ROOT_PATH, rep_id, f)

def tag_id_2_piece_id(tag_id):
    ''' retourne l'identidiant  de la piece associé au tag_id '''
    logging.getLogger().debug("        tag_id_2_piece_id(tag_id=%s):" % (tag_id))
    if tag_id is not None:
        logging.getLogger().debug("        tag_id_2_piece_id(tag_id=%s->tag_id[-8:].lower()=%s):" % (tag_id, tag_id[-8:].lower() ))
        for piece_id in pieces:
            for lt in pieces[piece_id]:
                if tag_id[-8:].lower() in lt:
                    logging.getLogger().debug("        tag_id=%s => piece_id=%s" % (tag_id, piece_id))
                    return piece_id
    else:
        logging.getLogger().debug("        PAS_DE_PIECE")
    return None

if __name__ == '__main__':
    import argparse
    import sys

    import utils

    parser = argparse.ArgumentParser(description='Verifier les pieces')
    parser.add_argument('-s', '--sound',
                        action='store_true',
                        help='with sound?',
                        default=False)
    parser.add_argument('-i', '--idpiece',
                        type=str,
                        help='id de la piece')
    args = parser.parse_args()

    init()

    all_tags = []
    if args.idpiece :
        p_id_list = [args.idpiece]
    else:
        p_id_list = pieces.keys()
        p_id_list.sort()
    for p_id in p_id_list:
        print "* %s" % (p_id)
        for tags in pieces[p_id]:
            print "  %s " % (tags)
            for tag in tags:
                if tag not in all_tags:
                    all_tags.append(tag)
                else:
                    print "  ERREUR: tags '%s' déjà présent" % tag

        if args.sound and not utils.music.play(get_audio_filename(p_id), wait_for_the_end=True):
            print "Erreur de fichier audio: %s" % get_audio_filename(p_id)

    print "Nombre de pieces : %d " % len(p_id_list)
    print "Nombre de tags   : %d " % len(all_tags)
    sys.exit(0)
