# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import logging
import sm130wrapper

class Switch(object):
    def __init__(self):
        logging.getLogger().debug("__init__")
        self.current_colonne = 0
        self.current_antenne = 0

    def allumer_antenne(self, num):
        logging.getLogger().debug("          allumer_antenne num=%d" % num)
        self.current_antenne = num

    def selectionner_colonne(self, i_col):
        logging.getLogger().debug("   selectionner_colonne i_col=%d" % i_col)
        self.current_colonne = i_col

class Bouton(object):
    def __init__(self):
        pass

    def appuye(self):
        pass

    def relache(self):
        sm130wrapper.attendreBouton()
        return True
