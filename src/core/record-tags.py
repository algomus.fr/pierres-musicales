# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import defs
import sm130wrapper
import utils

def main():
    num_tag = 1
    tags = []
    if sm130wrapper.initPlateau(defs.I2C_ADDR) < 0:
        print "ERREUR initPlateau"
        return -1

    while True:
        etat = sm130wrapper.getEtatTapis()
        for ( (i_col, i_ant), tag) in etat:
            if tag is not None and tag not in tags:
                print "(i_col, i_ant, tag= = (%s, %s, %s)" % (i_col, i_ant, tag)
                #tags.append(tag)
                num_tag += 1
        utils.delay (1000)

if __name__ == '__main__':
    main()
