# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import utils
import pieces
import defs

dictees_melodie = {}
dictees_notes = {}
audio_filenames = {}

for dictees_module in defs.list_dictees:
    try:
        dictees_data = __import__(dictees_module)
        dictees_melodie.update(dictees_data.dictees_melodie 
                               if dictees_data.dictees_melodie is not None else {})
        dictees_notes.update(dictees_data.dictees_notes 
                             if dictees_data.dictees_notes is not None else {})
        audio_filenames.update(dictees_data.audio_filenames 
                               if dictees_data.audio_filenames is not None else {})
    except ImportError:
        print("WARNING: pas de module %s" % dictees_module)

dictees = dict(dictees_melodie.items() + dictees_notes.items())

def get_dictees_ids():
    return dictees.keys()

def get_dictees_ennonce_piece_ids():
    dictees_ids = []
    for d in dictees:
        (dictee_id, _) = dictees[d]
        dictees_ids.append(dictee_id)
    return dictees_ids

def ennonce_piece_ids_2_dictee_id(ennonce_piece_ids):
    for d in dictees:
        (dictee_id, _) = dictees[d]
        if  dictee_id == ennonce_piece_ids:
            return d
    return None

def get_piece_id_ennonce(dictee_id):
    if dictee_id in dictees:
        return dictees[dictee_id][0]
    else:
        return None

def get_piece_id_solutions(dictee_id):
    if dictee_id in dictees:
        return dictees[dictee_id][1]
    else:
        return None

if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser(description='Verifier les dictees')
    parser.add_argument('-s', '--sound',
                        action='store_true',
                        help='with sound?',
                        default=False)
    parser.add_argument('-i', '--idjeu',
                        type=int,
                        help='id du jeu')
    args = parser.parse_args()

    pieces.init()

    if args.idjeu:
        dictee_id_list = [args.idjeu]
    else:
        dictee_id_list = dictees.keys()
        dictee_id_list.sort()
    print dictee_id_list
    for d_id in dictee_id_list:
        dictee_ennonce = get_piece_id_ennonce(d_id)
        dictee_soluces = get_piece_id_solutions(d_id)

        print("* %s : '%s'" %(d_id, dictee_ennonce))
        if args.sound and not utils.music.play(pieces.get_audio_filename(dictee_ennonce, audio_filenames=audio_filenames), wait_for_the_end=True):
            print("Erreur de fichier audio: %s" % pieces.get_audio_filename(dictee_ennonce, audio_filenames=audio_filenames))

        for soluce in dictee_soluces:
            print("  - %s " % (soluce))
            if args.sound:
                for piece_id in soluce:
                    if not utils.music.play(pieces.get_audio_filename(piece_id, audio_filenames=audio_filenames), wait_for_the_end=True):
                        print("Erreur de fichier audio: %s" % pieces.get_audio_filename(piece_id, audio_filenames=audio_filenames))
    print("Nombre de dictees : %d " % len(dictee_id_list))
    sys.exit(0)
