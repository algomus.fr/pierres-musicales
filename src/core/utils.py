# -*- coding: utf-8 -*-
# This file is part of "Les pierres musicales" <http://www.algomus.fr/pierres>
# Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS
# Contributors: Mathieu Giraud, Richard Groult, Nicolas Guiomard-Kagan,
#               Emmanuel Leguy, Florence Levé, Hervé Midavaine

# "Les pierres musicales" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Les pierres musicales" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>

import logging
import time
import pygame # see python-pygame

class Music(object):
    def __init__(self):
        pygame.mixer.init()

    def play(self, filename, wait_for_the_end=False):
        if self.is_playing():
            self.stop()
        try:
            logging.getLogger().debug("chargement %s... " % filename)
            pygame.mixer.music.load(filename)
            logging.getLogger().debug("joue %s..." % filename)
            pygame.mixer.music.play()
            if wait_for_the_end:
                self._wait_end_of_playing()
            return True

        except RuntimeError:
            logging.getLogger().debug("erreur de chargement de %s" % filename)
            return False

    def _wait_end_of_playing(self):
        while self.is_playing():
            delay(500)

    def is_playing(self):
        return pygame.mixer.music.get_busy()

    def stop(self):
        pygame.mixer.music.fadeout(1000)
        self._wait_end_of_playing()

music = Music()
def jouer(mp3):
    music.play(mp3)

def delay(ms):
    time.sleep(1.0 * ms / 1000)
