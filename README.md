# Les pierres musicales
*Un dispositif interactif tangible pour expérimenter la construction de la musique*

<http://www.algomus.fr/pierres>

Version 2.2


## Contenus

- Sur le git
  - `src/core`: sources du programme Arduino

- Téléchargeable depuis <http://www.algomus.fr/pierres>
  - `audio/`: `.ogg` (enregistrements piano par F. Levé et cordes par J. Debove)
  - `designs/`:
     - "À la recherche des Pierres Musicales", O. Hérouart (2016)
     - partitions (`.pdf`)
     - étiquette (`.pdf)
  - `schemas/`: .pcb et .pdf des circuits du boitier


## Programme Arduino

### Installation

Les pierres musicales ont été testées avec un pcDuino V1.

Packages à installer:
  - sound-theme-freedesktop
  - python-dev
  - python-pygame


### Exécution

```
cd src/core
python pierresMusicales.py
```


## Licences

- `audio/`, `designs/`, `schemas/`: **CC BY-SA 4.0**

  *This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.*

- `src/`: **GPL v3+**

  *"Les pierres musicales" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. "Les pierres musicales" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with "Les pierres musicales". If not, see <http://www.gnu.org/licenses/>*

- Sections on GPIO in `src/core/rfid-pcduino.c` are based on the example code (beerware) by Mike Hord https://raw.githubusercontent.com/sparkfun/pcDuino/master/examples/gpio/gpio_test.cpp


## Crédits et remerciements

J. Debove et al., Les pierres musicales, un dispositif pédagogique interactif,
Journéees d'Informatique Musicale (JIM 2017), Paris.
<https://hal.archives-ouvertes.fr/hal-01517519/>


Copyright (C) 2014, 2015, 2016, 2017 by Algomus at CRIStAL/MIS

Contributeurs:
  Julien Debove, David Durand, Marion Giraud, Mathieu Giraud,
  Richard Groult, Nicolas Guiomard-Kagan, Ophélie Hérouart, Emmanuel Leguy,
  Florence Levé, Nathan Marécaux, Hervé Midavaine

Les pierres musicales ont été réalisées grâce au soutien des laboratoire MIS et CRIStAL, 
du programme Sciences et Cultures du Visuel de l'Université de Lille, d'un soutien 
Expériences Interactives du fonds régional Pictanovo et à des dons privés. Nous remercions 
le FabLab Étoele pour la réalisation des pierres, les étudiants de l'IUT d'Amiens pour la 
réalisation de pièces prototypes dans le FabLab de l'IUT, ainsi que la plateforme de 
l'IRCICA pour la gravure de circuits prototypes.
